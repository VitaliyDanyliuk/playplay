# Generated by Django 4.2.1 on 2023-05-27 08:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('js_games', '0003_game_description'),
    ]

    operations = [
        migrations.RenameField(
            model_name='game',
            old_name='description',
            new_name='js_game_description',
        ),
        migrations.RenameField(
            model_name='game',
            old_name='game_enabled',
            new_name='js_game_enabled',
        ),
    ]
