# Generated by Django 4.2.1 on 2023-05-26 09:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('js_game_name', models.CharField(max_length=100, verbose_name='Назва гри')),
                ('js_game_file', models.FileField(upload_to='', verbose_name='Шлях гри')),
                ('js_game_image', models.ImageField(blank=True, null=True, upload_to='js_games/', verbose_name='Фото гри')),
            ],
        ),
    ]
