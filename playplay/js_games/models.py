from django.db import models


class Game(models.Model):
    js_game_name = models.CharField(max_length=100, verbose_name='Назва гри')
    js_game_file = models.FileField(verbose_name='Шлях гри')
    js_game_image = models.ImageField(upload_to='js_games/', blank=True, null=True, verbose_name='Фото гри')
    js_game_description = models.TextField(blank=True, null=True, verbose_name='Короткий опис гри')
    js_game_enabled = models.BooleanField(default=False, verbose_name='Включити/Вимкнути')
    js_game_width = models.IntegerField(blank=True, null=True, verbose_name='Ширина гри')
    js_game_height = models.IntegerField(blank=True, null=True, verbose_name='Висота')


    def __str__(self):
        return self.js_game_name