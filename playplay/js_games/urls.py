from django.urls import path
from . import views

app_name = 'js_games'
urlpatterns = [
    path('', views.js_gamse_list, name='js_gamse_list'),
    path('play_game/?<str:game_id>/', views.play_gamse, name='play_game'),
]
