from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from . import models


def index(request: HttpRequest):
    return render(request, 'js_games/base_for_js_games.html')


def js_gamse_list(request: HttpRequest):
    games = models.Game.objects.filter(js_game_enabled=1)
    return render(request, 'js_games/js_games_list.html', {'games': games})


def play_gamse(request: HttpRequest, game_id:str=None):
    game_objct = models.Game.objects.get(pk=game_id)
    game_url = '/static/js_games' + game_objct.js_game_file.url
    game_width = game_objct.js_game_width
    game_height = game_objct.js_game_height
    context = {
        'game_url': game_url,
        'game_width': game_width,
        'game_height': game_height,
    }
    return render(request, 'js_games/base_for_js_games.html', context)
