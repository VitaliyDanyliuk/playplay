from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpRequest
from personnel_hub.models import Product, Order, OrderItem
from django.contrib.auth.models import User
from .cart import Cart
from .forms import CartAddProductForm
from django.views.decorators.csrf import csrf_protect
from django.utils.timezone import now
from django.contrib.auth.decorators import login_required


@login_required(login_url='auth_app:login')
def cart_add(request:HttpRequest, product_id:int):
    cart = Cart(request)
    product = Product.objects.get(pk=product_id)
    cart.add(product=product, quantity=1, update_quantity=False)
    return redirect('cart:cart_detail')


@login_required(login_url='auth_app:login')
def cart_remove(request:HttpRequest, product_id:int):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')


@login_required(login_url='auth_app:login')
def cart_detail(request:HttpRequest):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(initial={'quantity': item['quantity'],
                                                                   'update': True})
    return render(request, 'cart/detail.html', {'cart': cart})


@login_required(login_url='auth_app:login')
def order(request:HttpRequest):
    return render(request, 'cart/order_detail.html')


@csrf_protect
def confirm_order(request: HttpRequest):
    '''
    Підтвердження замовлення, формується обєкт Order та пов'язані з ним OrderItem
    Якщо на складі недостатня кількість товару/товарів - перенеправляємо клієнта на order_problems.html
    Клієнт має в корзині відредагувати замовлення
    '''

    # Перевірка необхідної кількості товару на складі. Формування проблемного контексту

    product_check_list = []
    cart = Cart(request)
    for item in cart:
        product_instance = Product.objects.get(pk=item['product'].id)
        if product_instance.quantity_in_stock < item['quantity']:
            product_check_list.append(product_instance)
    context = {'product_check_list': product_check_list}

    # Визначення сценарію подальшої обробки

    if not(product_check_list): # сценарій, коли товару на складі достатньо, для виконання замовлення
        user_instance = User.objects.get(pk=request.user.id)
        order = Order(
            user=user_instance,
            order_date=now(),
            total_amount=cart.get_total_price()
        )
        order.save()
        order_instance = Order.objects.get(pk=order.id)

        for item in cart:
            product_instance = Product.objects.get(pk=item['product'].id)
            orderitem = OrderItem(
                order=order_instance,
                product=product_instance,
                quantity=item['quantity']
            )
            cart.remove(product_instance)
            orderitem.save()
            product_instance.quantity_in_stock -= item['quantity']
            product_instance.save()
        return render(request, 'cart/confirm_order_done.html')
    else: # сценарій, коли недостатньо товару
        return render(request, 'cart/order_problems.html', context=context)

