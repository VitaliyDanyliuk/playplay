from django.apps import AppConfig


class PersonnelHubConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'personnel_hub'
