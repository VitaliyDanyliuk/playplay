from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Назва категорії')
    description = models.TextField(verbose_name='Опис категорії')
    staff_id = models.IntegerField(blank=True, null=True, verbose_name='Ід працівника')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name='Назва товару')
    description = models.TextField(verbose_name='Короткий опис товару')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Ціна за одиницю товару')
    image = models.ImageField(upload_to='products/', blank=True, null=True, verbose_name='Фото/картинка товару')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категорія товару')
    quantity_in_stock = models.PositiveIntegerField(default=0, verbose_name='Кількість на складі')
    staff_id = models.IntegerField(blank=True, null=True, verbose_name='Ід працівника')
    genre = models.CharField(blank=True, null=True, max_length=100, verbose_name='Жанр')
    localization = models.CharField(blank=True, null=True, max_length=100, verbose_name='Локалізація')
    release_date = models.DateField(blank=True, null=True, verbose_name='Дата світового релізу')
    age_rating = models.CharField(blank=True, null=True, max_length=15, verbose_name='Вікові обмеження')
    system_requirements = models.TextField(blank=True, null=True, verbose_name='Рекомендовані системні вимоги')
    country_of_origin = models.CharField(blank=True, null=True, max_length=100, verbose_name='Країна-виробник')
    special_features = models.TextField(blank=True, null=True, verbose_name='Особливості')
    player_count = models.PositiveIntegerField(blank=True, null=True, verbose_name='Кількість гравців')
    publisher = models.CharField(blank=True, null=True, max_length=100, verbose_name='Видавець')
    developer = models.CharField(blank=True, null=True, max_length=100, verbose_name='Розробник')
    country_of_brand_registration = models.CharField(blank=True, null=True, max_length=100, verbose_name='Країна реєстрації бренду')

    def __str__(self):
        return self.name


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order_date = models.DateTimeField(auto_now_add=True)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)

    
class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()


class Review(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.PositiveIntegerField()
    comment = models.TextField()


class Discount(models.Model):
    code = models.CharField(max_length=50, unique=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)


