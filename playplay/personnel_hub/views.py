from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpRequest
from django.views import View
from . import forms, models
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator


def is_staff(user):
    return user.is_staff


@user_passes_test(is_staff, login_url='main_page:index')
def index(request: HttpRequest):
    return render(request, 'personnel_hub/personnel_hub.html')


@method_decorator(user_passes_test(is_staff, login_url='main_page:index'), name='dispatch')
class ProductView(View):
    def get(self, request: HttpRequest):
        form = forms.ProductForm
        return render(request, 'personnel_hub/product_add_form.html', {'form': form, 'title_name': 'Продукти'})

    def post(self, request: HttpRequest):
        form = forms.ProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.staff_id = request.user.id
            product.save()
            form = forms.ProductForm()
            return redirect('personnel_hub:products')
        return render(request, 'personnel_hub/product_add_form.html', {'form': form})


@user_passes_test(is_staff, login_url='main_page:index')
def products(request: HttpRequest):
    products = models.Product.objects.all()
    return render(request, 'personnel_hub/products.html', {'products': products})


@user_passes_test(is_staff, login_url='main_page:index')
def category(request: HttpRequest):
        categories = models.Category.objects.all()
        content = {'categories': categories}
        return render(request, 'personnel_hub/categories.html', content)


@user_passes_test(is_staff, login_url='main_page:index')
def orders(request: HttpRequest):
    orders_set = models.Order.objects.all()
    return render(request, 'personnel_hub/orders.html', {'orders': orders_set})


@method_decorator(user_passes_test(is_staff, login_url='main_page:index'), name='dispatch')
class ProductChangeQuantity(View):
    def get(self, request: HttpRequest, pk: int):
        product = get_object_or_404(models.Product, pk=pk)
        form = forms.ProductChangeQuantityForm(instance=product)
        context = {
            'form': form,
            'product': product,
        }
        return render(request, 'personnel_hub/product_change_form.html', context)

    def post(self, request: HttpRequest, pk: int):
        product = get_object_or_404(models.Product, pk=pk)
        form = forms.ProductChangeQuantityForm(request.POST, instance=product)
        if form.is_valid():
            product = form.save(commit=False)
            product.staff_id = request.user.id
            product.save()
            return redirect('personnel_hub:products')
        else:
            context = {
                'form': form,
                'product': product,
            }
            return render(request, 'personnel_hub/product_change_form.html', context)


@method_decorator(user_passes_test(is_staff, login_url='main_page:index'), name='dispatch')
class ProductChange(View):
    def get(self, request: HttpRequest, pk: int):
        product = get_object_or_404(models.Product, pk=pk)
        form = forms.ProductForm(instance=product)
        context = {
            'form': form,
            'product': product,
        }
        return render(request, 'personnel_hub/product_add_form.html', context)

    def post(self, request: HttpRequest, pk: int):
        product = get_object_or_404(models.Product, pk=pk)
        form = forms.ProductForm(request.POST, request.FILES, instance=product)
        if form.is_valid():
            print('ok')
            product = form.save(commit=False)
            product.staff_id = request.user.id
            product.save()
            return redirect('personnel_hub:products')
        else:
            context = {
                'form': form,
                'product': product,
            }
            return render(request, 'personnel_hub/product_add_form.html', context)

