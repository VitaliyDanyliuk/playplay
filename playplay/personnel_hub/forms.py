from . import models
from django import forms


class ProductForm(forms.ModelForm):
    class Meta:
        model = models.Product
        exclude = ['staff_id']
        widgets = {
            'release_date': forms.DateInput(attrs={'type': 'date'}),
        }

class ProductChangeQuantityForm(forms.ModelForm):
    class Meta:
        model = models.Product
        fields = ['quantity_in_stock']


class CategoryForm(forms.ModelForm):
    class Meta:
        model = models.Category
        exclude = ['staff_id']
