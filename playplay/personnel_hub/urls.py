from django.urls import path
from . import views

app_name = 'personnel_hub'
urlpatterns = [
    path('', views.index, name='personnel_hub'),
    path('products/', views.products, name='products'),
    path('products/add_new_product', views.ProductView.as_view(), name='add_new_product'),
    path('categories/', views.category, name='categories'),
    path('orders/', views.orders, name='orders'),
    path('product/edit/<int:pk>/', views.ProductChange.as_view(), name='product_edit'),
    path('product/edit/quantity/<int:pk>/', views.ProductChangeQuantity.as_view(), name='product_edit_quantity'),
]
