from django.urls import path, reverse_lazy
from . import views
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordChangeDoneView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView
)


app_name = "auth_app"
urlpatterns = [
    path("login/", LoginView.as_view(template_name="auth_app/login.html"),  name="login"),
    path("logout/", LogoutView.as_view(), name="logout" ),
    path("password_change/", PasswordChangeView.as_view(
                template_name="auth_app/password_change_form.html",
                success_url=reverse_lazy("auth_app:password_change_done"),
                ), name="password_change"),
    path("password_change/done/", PasswordChangeDoneView.as_view(
        template_name="auth_app/password_change_done.html"),
        name="password_change_done",
        ),
    path("password_reset/", views.CustomPasswordResetView.as_view(
        template_name='auth_app/password_reset_form.html',
        success_url = reverse_lazy("auth_app:password_reset_done"),
        email_template_name = "auth_app/password_reset_email.html"
        ), name="password_reset"),
    path(
        "password_reset/done/",
        PasswordResetDoneView.as_view(template_name='auth_app/password_reset_done.html'),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/", PasswordResetConfirmView.as_view(
            success_url=reverse_lazy("auth_app:password_reset_complete"),
            template_name="auth_app/password_reset_confirm.html"
        ),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        PasswordResetCompleteView.as_view(
            template_name="auth_app/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path("register/", views.register, name="register"),
]