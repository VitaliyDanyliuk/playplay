from django.shortcuts import render, redirect, reverse
from django.contrib.auth import login
from django.contrib.auth.models import User
from .forms import CustomUserCreationForm
from django.contrib.auth.views import PasswordResetView
from django.urls import reverse_lazy
from django.core.mail import send_mail
from django.conf import settings
from decouple import config

def register(request):
    if request.method == "GET":
        return render(request, "auth_app/auth_registration.html", {"form": CustomUserCreationForm})

    elif request.method == "POST":
        username = request.POST['username']
        if User.objects.filter(username=username).exists():
            return render(
                            request, "auth_app/auth_registration.html",
                            {'error': f'Користувач {username} вже існує. Спробуйте ще раз.',
                             'form': CustomUserCreationForm})
        else:
            form = CustomUserCreationForm(request.POST)
            if form.is_valid():
                user = form.save()
                login(request, user)
                subject = 'Дякуємо за реєстрацію!'
                message = 'Шановний {},\n\nДякуємо, що зареєструвалися на нашому сайті. ' \
                          'Будь ласка, зверніться до нас, якщо у вас виникнуть питання або потрібна допомога.' \
                          '\n\nЗ повагою,\nКоманда сайту {}'.format(user.username, config('PLAYPLAY_URL'))
                from_email = settings.EMAIL_HOST_USER
                to_email = [user.email]
                try:
                    send_mail(subject, message, from_email, to_email)
                except:
                    pass
                return redirect(reverse("main_page:index"))


class CustomPasswordResetView(PasswordResetView):
    email_template_name = 'auth_app/password_reset_email.html'
    success_url = reverse_lazy('password_reset_done')

    def form_valid(self, form):
        login = form.cleaned_data['email']
        try:
            user = User.objects.get(email=login)
            username = user.username
        except User.DoesNotExist:
            username = None
        self.extra_email_context = {
            'username': username,
        }
        return super().form_valid(form)
