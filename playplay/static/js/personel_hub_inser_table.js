function toggleTable(id, event) {
  event.preventDefault();
  var nestedTable = document.getElementById(id);
  if (nestedTable.style.display === "none") {
    nestedTable.style.display = "table";
  } else {
    nestedTable.style.display = "none";
  }
}
