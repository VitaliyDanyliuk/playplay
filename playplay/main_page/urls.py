from django.urls import path
from . import views

app_name = 'main_page'
urlpatterns = [
    path('', views.index, name='index'),
    path('products_list/?<str:category>', views.products_list, name='products_list'),
    path('product/?<str:product_id>', views.product, name='product'),
    path('about_us/', views.about_us, name='about_us'),
    path('contacts/', views.contacts, name='contacts'),
]
