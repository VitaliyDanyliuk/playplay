from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from personnel_hub import models
from cart.forms import CartAddProductForm
from django.db.models import Sum
from django.conf import settings


def index(request: HttpRequest) -> HttpResponse:
    top_count_products = getattr(settings, 'TOP_PRODUCTS_COUNT')
    top_products = models.OrderItem.objects.values('product').annotate(total_quantity=Sum('quantity')).order_by(
        '-total_quantity')[:top_count_products]
    products = models.Product.objects.filter(id__in=[item['product'] for item in top_products])
    category_ids = [product.category_id for product in products]
    categories = models.Category.objects.filter(id__in=category_ids)
    return render(request, 'main_page/index.html', {'products': products, 'categories': categories})


def about_us(request: HttpRequest) -> HttpResponse:
    return HttpResponse(render(request, 'main_page/about_us.html'))


def contacts(request: HttpRequest) -> HttpResponse:
    return HttpResponse(render(request, 'main_page/contacts.html'))


def products_list(request: HttpRequest, category:str=None):
    category_id = models.Category.objects.get(name=category)
    products = models.Product.objects.filter(category=category_id.pk)
    return render(request, 'main_page/products_list.html', {'category_name': category, 'products_list':products})


def product(request: HttpRequest, product_id:str=None) -> HttpResponse:
    cart_product_form = CartAddProductForm()
    show_filed_list = [
        'description',
        'country_of_brand_registration',
        'country_of_origin',
        'developer',
        'genre',
        'localization',
        'player_count',
        'publisher',
        'release_date',
        'special_features',
        'system_requirements',
        'age_rating',
    ]
    product_item = models.Product.objects.get(pk=product_id)
    fields = {field.name: field.verbose_name for field in product_item._meta.get_fields() if field.name in show_filed_list}
    fields_values_dict = {field: product_item.serializable_value(field_name=field) for field in show_filed_list}
    render_fileds = {}
    for i in show_filed_list:
        render_fileds[fields[i]] = fields_values_dict[i]
    context = {'product': product_item, 'render_fileds': render_fileds, 'cart_product_form': cart_product_form}
    return HttpResponse(render(request, 'main_page/product.html', context))
