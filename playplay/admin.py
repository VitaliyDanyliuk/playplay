import shutil
import sys
import os
from datetime import datetime
from decouple import config


def backup_database():
    '''
    функція виконує бекап файлу бази даних.
    Шлях до бази та назва  беруться з файлу .env (DB_PATH, DB_NAME)
    Шлях для зберігання файлу бекап береться також з файлу .env (DB_BACKUP_PATH)
    Ім'я файлу бекапу генериться backup_%Y-%m-%d_%H-%M-%S_DB_NAME
    Функція не приймає аргументів
    '''
    db_path = f"{config('DB_PATH')}{config('DB_NAME')}"
    current_datetime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    backup_file_name = f"backup_{current_datetime}_{config('DB_NAME')}"
    backup_path = os.path.join(config('DB_BACKUP_PATH'), backup_file_name)
    shutil.copy2(db_path, backup_path)

    print(f"Бекап бази даних створено успішно. Назва файлу бекапу: {backup_file_name}")


def restore_database(backup_file):
    '''
    функція виконує відновлення бази даних з бекап-файлу.
    Шлях до бази та назва  беруться з файлу .env (DB_PATH, DB_NAME)
    Шлях  файлу бекап береться також з файлу .env (DB_BACKUP_PATH)
    Функція приймає параметр backup_file - тут вказується імя бекап-файлу, з якого буде відновлено базу
    '''
    backup_file = os.path.join(config('DB_BACKUP_PATH'), backup_file)
    db_restore_path = os.path.join(config('DB_PATH'), config('DB_NAME'))

    if os.path.isfile(backup_file):
        shutil.copy2(backup_file, db_restore_path)
        print("База даних відновлена з бекапу.")
    else:
        print("Файл бекапу не знайдено.")


args = sys.argv[1:]


if len(args) == 1 and args[0] == 'base_backup':
    backup_database()
elif len(args) == 2 and args[0] == 'base_restore':
    backup_file = args[1]
    restore_database(backup_file)
if len(args) == 1 and args[0] == 'help':
    print('backup_database'.center(20, '*'))
    print(help(backup_database))
    print('restore_database'.center(20, '*'))
    print(help(restore_database))

else:
    print("Некоректні аргументи командного рядка.")
